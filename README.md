== Tokenizer ==

This project allows for the secure tokenization of credit card data through the E4 Payment Gateway

USAGE:

1.  Configure gateway credentials
2.  Run tokenize <cc_num> / <exp_date>
3.  Receive token back on success or error message
