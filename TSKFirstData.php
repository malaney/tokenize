<?php
require 'vendor/autoload.php';

use VinceG\FirstDataApi\FirstData;

class TSKFirstData extends FirstData
{

	/**
   	* Makes an HTTP request. This method can be overriden by subclasses if
   	* developers want to do fancier things or use something other than curl to
   	* make the request.
   	*
   	* @param CurlHandler optional initialized curl handle
   	* @return String the response text
   	*/
  	protected function doRequest($ch=null) {
    	if (!$ch) {
      		$ch = curl_init();
    	}
		$opts = self::$CURL_OPTS;
		$content = json_encode(array_merge($this->getPostData(), array('gateway_id' => $this->username, 'password' => $this->password, 'transaction_type' => $this->transactionType)));
		$opts[CURLOPT_POSTFIELDS] = $content;
		$opts[CURLOPT_URL] = self::$testMode ? self::TEST_API_URL . $this->apiVersion : self::LIVE_API_URL . $this->apiVersion;
		if ($this->apiVersion >= 'v12') {
			$gge4_date = gmdate('Y-m-d\TH:i:s') . 'Z'; // ISO8601
			$content_digest = sha1($content);
			$hmac = base64_encode(hash_hmac('sha1',
				"POST\n" .
				"application/json; charset=UTF-8;\n" .
				$content_digest . "\n" .
				$gge4_date . "\n" .
				"/transaction/" . $this->apiVersion,
				$this->apiKey, true));
			$headers = array(
				'X-GGe4-Content-SHA1: ' . $content_digest,
				'X-GGe4-Date: ' . $gge4_date,
				'Authorization: GGE4_API ' . $this->apiId . ':' . $hmac,
				'Content-Length: ' . strlen($content)
			);
			$opts[CURLOPT_HTTPHEADER] = array_merge($opts[CURLOPT_HTTPHEADER], $headers);
		}
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt ($ch, CURLOPT_CAINFO, dirname(__FILE__)."/cacert.pem");

		// set options
		curl_setopt_array($ch, $opts);

		// execute
		$this->setResponse( curl_exec($ch) );
		$this->setHeaders( curl_getinfo($ch) );

		// fetch errors
		$this->setErrorCode( curl_errno($ch) );
		$this->setErrorMessage( curl_error($ch) );

		// Convert response to array
		$this->convertResponseToArray();

		// We need to make sure we do not have any errors
		if(!$this->getArrayResponse()) {
			// We have an error
			$returnedMessage = $this->getResponse();

			// Pull out the error code from the message
			preg_match('/\(([0-9]+)\)/', $returnedMessage, $matches);

			$errorCodes = $this->getResponseCodes();

			if(isset($matches[1])) {
				// If it's not 00 then there was an error
				$this->setErrorCode( isset($errorCodes[$matches[1]]) ? $matches[1] : 42 );
				$this->setErrorMessage( isset($errorCodes[$matches[1]]) ? $errorCodes[$matches[1]] : $errorCodes[42] );
			} else {
				$headers = $this->getHeaders();
				$this->setErrorCode($headers['http_code']);
				$this->setErrorMessage($returnedMessage);
			}
		} else {
			// We have a json string, empty error message
			$this->setErrorMessage('');
			$this->setErrorCode(0);
		}

		// close
		curl_close($ch);

		// Reset
		$this->postFields = array();

		return $this->getResponse();
    }
}
