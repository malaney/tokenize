<?php
require 'vendor/autoload.php';
require 'mapreduce.php';
require 'TSKFirstData.php';

//  use VinceG\FirstDataApi\FirstData;
use Ulrichsg\Getopt\Getopt;
use Ulrichsg\Getopt\Option;
use Ulrichsg\Getopt\Argument;

$getopt = new Getopt(array(
    (new Option('f', 'file', Getopt::REQUIRED_ARGUMENT))
        ->setDescription('input file containing bpa data in csv format')
        ->setValidation(function($value) {
            return is_readable($value);
        }),
    (new Option('o', 'output', Getopt::REQUIRED_ARGUMENT))
        ->setDescription('output file to save tokenized data to (default: output.csv)')
        ->setDefaultValue('output.csv'),
    (new Option('l', 'login', Getopt::REQUIRED_ARGUMENT))
        ->setDescription('E4 api login')
        ->setValidation(function($value) {
            return is_string($value);
        }),
    (new Option('k', 'key', Getopt::REQUIRED_ARGUMENT))
        ->setDescription('E4 api key')
        ->setValidation(function($value) {
            return is_string($value);
        }),
    (new Option('v', 'live', Getopt::OPTIONAL_ARGUMENT))
        ->setDescription('Live mode 1 = live, 0 = test, default = 0')
        ->setValidation(function($value) {
            return is_numeric($value);
        }),

));

try {
    $getopt->parse();
    $file = $getopt['file'];
    $output = $getopt['output'];
    $login = $getopt['login'];
    $key = $getopt['key'];
    $live = (int) $getopt['live'];
    $debugmode = ($live != 1) ? true:false;

    if (!$file) {
        throw new \Exception('File must be provided');
    }
    if (!$output) {
        $output = 'output.csv';
    }
    if (!$login) {
        throw new \Exception('Login must be provided');
    }
    if (!$key) {
        throw new \Exception('Key must be provided');
    }
} catch (\Exception $e) {
    print "Error: " . $e->getMessage() . "\n";
    print $getopt->getHelpText();
    exit(1);
}

$data = file_get_contents($file);
$lines = explode(PHP_EOL, $data);

$count = 0;
$skip = 1;
$results = [];

foreach ($lines as $line) {
    if ($line) {
        if ($count >= $skip) {
            $i = 0;
            $parts = preg_split('/,/', trim($line));
            list($legacy_student_id, $student_first_name, $student_last_name, $legacy_contract_id, $payment_method, $credit_card_number, $expiration_date) = $parts;
            $results[] = map(function($parts) use ($i, $login, $key, $payment_method, $expiration_date, $student_first_name, $student_last_name, $debugmode) {
                        static $i;
                        $i++;
                        if ($i == 6) {
                            $firstData = new TSKFirstData($login, $key, $debugmode);
                            // var_dump($login);
                            // var_dump($key);
                            // var_dump($debugmode); 
                            // exit;
                            $firstData->setTransactionType(TSKFirstData::TRAN_PREAUTHONLY);
                            $firstData->setCreditCardType($payment_method)
                                    ->setCreditCardNumber($parts)
                                    ->setCreditCardName("$student_first_name $student_last_name")
                                    ->setCreditCardExpiration($expiration_date)
                                ;
                            
                            $result = $firstData->process();
                            if (!$firstData->isError()) {
                                $obj = json_decode($result);
                                if (!empty($obj->transarmor_token)) {
                                    return $obj->transarmor_token;
                                } else {
                                    // var_dump($obj);
                                    // print 'oops';
                                    print $result;
                                    print 'successful run, but no transarmor token found';
                                    exit;
                                }
                            } else {
                                // var_dump($result);
                                $obj = json_encode($result);
                                if (!is_object($obj)) {
                                    print $obj;
                                } else {
                                    return $obj->bank_message;
                                }
                                return 'XXXXXXXXXXX - dud - XXXXXXXXXXX';
                            }
                        } else {
                            return $parts;
                        }
                    }, 
                    $parts);
        }
        print '.'; // printing a dot for status ...
    }
    $count++;
}

if ($results) {
    $fp = fopen($output, 'w');
    foreach ($results as $result) {
        fputcsv($fp, $result);
    }
    fclose($fp);
}

