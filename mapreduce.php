<?php
// First element of an array  
function first($in) {   
    return empty($in) ? null : $in[0];  
}  
  
// Everything after the first element of an array  
function rest($in) {  
    $out = $in;  
    if (!empty($out)) { array_shift($out); }  
    return $out;  
}  

// Take an element and an array  
// and fuse them together so that the element  
// is at the front of the array  
function construct($first, $rest) {  
    array_unshift($rest, $first);  
    return $rest;  
}
    
function map($fn, $list) {
    return empty($list) ?
               array() :
               construct($fn(first($list)), map($fn, rest($list)));
}

function reduce($fn, $list, $identity) {  
    return empty($list) ? 
               $identity  :
               $fn(first($list),
                  reduce($fn, rest($list), $identity));
}



